const app = getApp();

Page({
    data: {
      phoneNumber: '15201137561',
        qqNumber: '3254610367',
        address: '北京市昌平区回龙观西大街东亚上北中心',
        transportLines: `乘坐地铁13号线，回龙观站下车，向北步行1.5公里到达;
                             公交车线路选择较多;`,
        markers: [{
            //终点
            iconPath: "/images/logo.png",
            id: 0,
          latitude: 40.087796,
          longitude: 116.348002, 
            width: 30,
            height: 30,
            callout: {
                content: '师兄帮',
                bgColor: 'rgba(0,0,0,.6)',
                display: 'BYCLICK',
                borderRadius: 5,
                borderColor: '#000',
                padding: 8,

            },

        },
        //用户当前位置

        {
            iconPath: "/images/address/location.png",
            id: 1,
            latitude: '',
            longitude: '',
            width: 30,
            height: 30,
            callout: {
                content: '您的位置',
                bgColor: 'rgba(0,0,0,.6)',
                display: 'BYCLICK',
                borderRadius: 5,
                borderColor: '#000',
                padding: 8,

            }
        }
        ],
        polyline: [{
            points: [
                {
                latitude: 40.087796,
                longitude: 116.348002,
                },
                {
                  latitude: 40.087796,
                  longitude: 116.348002
                }
            ],
            color: "#FF0000DD",
            width: 2,
            dottedLine: true,
            arrowLine: true
        }],
        // controls: [{
        //     id: 1,
        //     iconPath: '/resources/location.png',
        //     position: {
        //         left: 0,
        //         top: 300 - 50,
        //         width: 50,
        //         height: 50
        //     },
        //     clickable: true
        // }]
    },
    onLoad() {
        this.getUserLocation();

    },
    getUserLocation() {
        wx.getLocation({
            type: "wgs84",
            success: (res) => {
                console.log('用户位置', res);

                // this.setData({
                //   'transportLines': 'get location success'
                // });
                this.setData({
                    'markers[1].longitude': res.longitude,
                    'markers[1].latitude': res.latitude,
                    'polyline[0].points[1].longitude': res.longitude,
                    'polyline[0].points[1].latitude': res.latitude,
                })
                // console.log(this.data.markers)
                console.log(this.data.polyline)
            },
            fail: res => {
                // this.setData({
                //   'transportLines': 'get location failed'
                // });
                wx.showToast({
                    title: '请在设置中允许微信获取您的位置！',
                    icon: 'none',
                    duration: 2000,
                });
            }
        });
    },
    regionchange(e) {
        console.log(e.type)
    },
    markertap(e) {
        console.log(e.markerId)
    },
    controltap(e) {
        console.log(e.controlId)
    },
    //拨打电话
    callMe() {
        wx.makePhoneCall({
            phoneNumber: this.data.phoneNumber,
        })
    },
    //复制
    setClipboard(e) {
        wx.setClipboardData({
            data: e.target.dataset.text || '',
            success(res) {
                console.log(`已复制:${e.target.dataset.text || ''}`)
            }
        })
    }



})