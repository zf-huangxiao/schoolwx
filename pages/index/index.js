//获取应用实例
const app = getApp()

Page({
    data: {
        bannerList: [],
        showPics:false,//没法多个分开监视，所以一起动画
    
    },


    onLoad: function() {
        this.getBannerList();
        wx.getSystemInfo({
            success: res => {
                const SDKVersion = res.SDKVersion;
                this.handleLessonsPicAinmate(this.compareVersion(SDKVersion + '', '2.0.0'))
            },
        })

       


    },
    onUnload() {
        if (this.observer) this.observer.disconnect();
    },
    getBannerList() {
        wx.request({
            url: 'https://www.shixiongbang.cn/data/header.json',
            success: (res) => {
                console.log(res.data);
                var data = res.data;
                if (!data) return;
                this.setData({
                    bannerList: data
                })
            }
        })
    },

    compareVersion(v1, v2) {
        v1 = v1.split('.')
        v2 = v2.split('.')
        var len = Math.max(v1.length, v2.length)

        while (v1.length < len) {
            v1.push('0')
        }
        while (v2.length < len) {
            v2.push('0')
        }

        for (var i = 0; i < len; i++) {
            var num1 = parseInt(v1[i])
            var num2 = parseInt(v2[i])

            if (num1 > num2) {
                return 1
            } else if (num1 < num2) {
                return -1
            }
        }
        return 0
    },
    handleLessonsPicAinmate(compareRes){
        if(compareRes>=0){
            //基础库2.0.0之后才支持observeAll
            this.observer = wx.createIntersectionObserver(this, {
                thresholds:[0,1],
                observeAll: true
            }).relativeToViewport({
                bottom: 100
            });
            this.observer.observe('.pics', (res) => {
                if (res.intersectionRatio===1){
                    //动画区域出现在可视区
                    this.setData({
                        showPics: true,
                    });
                } else if (res.intersectionRatio === 0){
                    this.setData({
                        showPics: false,
                    })
                }
            });
        }else{
            this.setData({
                showPics:true,                  
            })

        }
    }
});