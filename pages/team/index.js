const app = getApp()

Page({
  data: {
    teachers:[],
    showingID:0,//正展示的教师
    showDesc:false,
    toView:''
  },

  onLoad: function () {
    this.getTeachersData();
  },
  scrollToView(e){
    this.setData({
      toView: `teacher${Number(e.currentTarget.dataset.index) + 1}`
    })
    console.log(this.data.toView)
  },
  showTeacherDetail(e){
    console.log(e)
    if (Number(e.currentTarget.dataset.index)===this.data.showingID){
      this.setData({
        showDesc: !this.data.showDesc
      })
    }else{
      this.setData({
        showingID: Number(e.currentTarget.dataset.index),
        showDesc:true,
      })
    }
  },
  getTeachersData(){
    wx.request({
      url: 'https://www.shixiongbang.cn/data/teacher.json',
      success:(res) =>{
        const data = res.data;
        if(!data) return;
        this.setData({
          teachers:data
        })
        console.log('教师:',data)
      }
    })
  }
});
